package com.xvidia.net.jimiwifi.network.model;

/**
 * Created by vasu on 4/10/16.
 */

public class CameraDetails {
    private String cameraName;


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    private String status;

    public String getCameraName() {
        return cameraName;
    }

    public void setCameraName(String cameraName) {
        this.cameraName = cameraName;
    }

    public CameraDetails() {
    }

    public CameraDetails(String name, String status) {
        this.cameraName = name;
        this.status = status;
    }

}