package com.xvidia.net.jimiwifi.adapter;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.xvidia.net.jimiwifi.fragments.SharedForOthersFragment;
import com.xvidia.net.jimiwifi.fragments.SharedWithMeFragment;

/**
 * Created by vasu on 7/3/17.
 */

public class TabsPagerAdapter extends FragmentPagerAdapter {

    public TabsPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int index) {

        switch (index) {
            case 0:
                // Top Rated fragment activity
                return new SharedForOthersFragment();
            case 1:
                // Games fragment activity
                return new SharedWithMeFragment();
        }

        return null;
    }

    @Override
    public int getCount() {
        // get item count - equal to number of tabs
        return 2;
    }

}