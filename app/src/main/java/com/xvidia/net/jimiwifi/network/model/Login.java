package com.xvidia.net.jimiwifi.network.model;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by vasu on 30/3/16.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class Login {

    public Login(String userName, String password) {
        this.userName = userName;
        this.password = password;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    private String userName;
    private String password;


}

