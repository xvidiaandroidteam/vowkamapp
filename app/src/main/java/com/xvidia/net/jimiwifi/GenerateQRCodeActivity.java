package com.xvidia.net.jimiwifi;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;

/**
 * Created by vasu on 29/9/16.
 */

public class GenerateQRCodeActivity extends AppCompatActivity {

    public final static int WIDTH=500;
    private ImageView imageView;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qrcode);
        initToolbars();
        imageView = (ImageView) findViewById(R.id.ivQRCode);
        Intent intent = getIntent();
        if(null != intent ){
            String wifi = intent.getStringExtra("WIFINAME");
            String password = intent.getStringExtra("PASSWORD");
            String qRCodeString = password+"#&#&"+wifi;
            try {
                Bitmap qrcode = encodeAsBitmap(qRCodeString);
                imageView.setImageBitmap(qrcode);

            } catch (WriterException e) {
                e.printStackTrace();
            }
        }
    }
    private void initToolbars() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setPadding(0, 0, 0, 0);
        toolbar.setTitle(R.string.login);
    }
    private Bitmap encodeAsBitmap(String str) throws WriterException {
        BitMatrix result;
        try {
            result = new MultiFormatWriter().encode(str,
                    BarcodeFormat.QR_CODE, WIDTH, WIDTH, null);
        } catch (IllegalArgumentException iae) {
            // Unsupported format
            return null;
        }
        int w = result.getWidth();
        int h = result.getHeight();
        int[] pixels = new int[w * h];
        for (int y = 0; y < h; y++) {
            int offset = y * w;
            for (int x = 0; x < w; x++) {
                pixels[offset + x] = result.get(x, y) ? getResources().getColor(android.R.color.black):getResources().getColor(android.R.color.white);
            }
        }

        Bitmap bitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
        bitmap.setPixels(pixels, 0, 500, 0, 0, w, h);
        return bitmap;
    } /// end of this method

}
