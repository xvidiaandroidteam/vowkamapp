package com.xvidia.net.jimiwifi.network.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by vasu on 7/9/16.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class CreateTrip {
    @JsonProperty("passengerPhoneNumber")
    private String passengerPhoneNumber;

    @JsonProperty("carRegNo")
    private String carRegNo;

    public String getPassengerPhoneNumber() {
        return passengerPhoneNumber;
    }

    public void setPassengerPhoneNumber(String passengerPhoneNumber) {
        this.passengerPhoneNumber = passengerPhoneNumber;
    }

    public String getCarRegNo() {
        return carRegNo;
    }

    public void setCarRegNo(String carRegNo) {
        this.carRegNo = carRegNo;
    }

    public String getStartTimeStamp() {
        return startTimeStamp;
    }

    public void setStartTimeStamp(String startTimeStamp) {
        this.startTimeStamp = startTimeStamp;
    }

    public String getStrId() {
        return strId;
    }

    public void setStrId(String strId) {
        this.strId = strId;
    }

    @JsonProperty("startTimeStamp")
    private String startTimeStamp;

    @JsonProperty("strId")
    private String strId;

}
