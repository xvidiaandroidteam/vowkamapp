package com.xvidia.net.jimiwifi.utils;

import com.dropbox.client2.session.Session;

/**
 * Created by vasu on 5/10/16.
 */

public class AppConstants {

    public final static long cacheHitButRefreshed = 05 * 1000; // in 30 seconds cache will be hit, but also refreshed on background
    public final static long cacheExpired = 10* 24 * 60 * 60 * 1000; // expiry time 10 days

    final static public String DROPBOX_APP_KEY = "s9mpmyt8cn08rr8";
    final static public String DROPBOX_APP_SECRET = "5my0xj8rcmgshbi";
    final static public Session.AccessType ACCESS_TYPE = Session.AccessType.DROPBOX;
}
