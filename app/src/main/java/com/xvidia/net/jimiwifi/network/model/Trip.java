package com.xvidia.net.jimiwifi.network.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by vasu on 3/5/16.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Trip {

    @JsonProperty("id")
    Long id;
    @JsonProperty("strId")
    String strId;
    @JsonProperty("fromLat")
    Float fromLat;
    @JsonProperty("fromLong")
    Float fromLong;
    @JsonProperty("toLat")
    Float toLat;
    @JsonProperty("toLong")
    Float toLong;
    @JsonProperty("startTimeStamp")
    Long startTimeStamp;
    @JsonProperty("stopTimeStamp")
    Long stopTimeStamp;
    @JsonProperty("otp")
    String otp;
    @JsonProperty("isActive")
    boolean isActive;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getStrId() {
        return strId;
    }

    public void setStrId(String strId) {
        this.strId = strId;
    }

    public Float getFromLat() {
        return fromLat;
    }

    public void setFromLat(Float fromLat) {
        this.fromLat = fromLat;
    }

    public Float getFromLong() {
        return fromLong;
    }

    public void setFromLong(Float fromLong) {
        this.fromLong = fromLong;
    }

    public Float getToLat() {
        return toLat;
    }

    public void setToLat(Float toLat) {
        this.toLat = toLat;
    }

    public Float getToLong() {
        return toLong;
    }

    public void setToLong(Float toLong) {
        this.toLong = toLong;
    }

    public Long getStartTimeStamp() {
        return startTimeStamp;
    }

    public void setStartTimeStamp(Long startTimeStamp) {
        this.startTimeStamp = startTimeStamp;
    }

    public Long getStopTimeStamp() {
        return stopTimeStamp;
    }

    public void setStopTimeStamp(Long stopTimeStamp) {
        this.stopTimeStamp = stopTimeStamp;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

}
