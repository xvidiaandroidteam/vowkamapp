package com.xvidia.net.jimiwifi.network.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by vasu on 8/9/16.
 */
@JsonIgnoreProperties(ignoreUnknown = true)

public class TripDetails {
    @JsonProperty("car")
    Car car;

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    public Trip getTrip() {
        return trip;
    }

    public void setTrip(Trip trip) {
        this.trip = trip;
    }

    @JsonProperty("trip")
    Trip trip;

    public Driver getDriver() {
        return driver;
    }

    public void setDriver(Driver driver) {
        this.driver = driver;
    }

    @JsonProperty("driver")
    Driver driver;
}
