package com.xvidia.net.jimiwifi;

import android.Manifest;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.util.Patterns;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.xvidia.net.jimiwifi.network.ServiceURLManager;
import com.xvidia.net.jimiwifi.network.VolleySingleton;

import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * A login screen that offers login via email/password.
 */
public class RegisterActivity extends AppCompatActivity {

    public static final int REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS = 123;
    public static final int REQUEST_CODE_ASK_PERMISSIONS = 125;
    // UI references.
    private AutoCompleteTextView mEmailView;
    private EditText mNumberView, mNameView;
    private EditText mPasswordView, mConfirmPasswordView;
    private ProgressBar progressBar;
    private ImageView mShowPassword, mShowPassword1;
    private static boolean disableFlag;
    private Button mEmailSignInButton;
    private static ArrayList<String> emails;
    private Toolbar toolbar;

    private Activity activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register);
        // Set up the login form.

//        SharedPreferences preferences = getSharedPreferences(DataStorage.STR_RESPONSE, 0);
//        SharedPreferences.Editor editor = preferences.edit();
//        editor.remove(DataStorage.STR_RESPONSE);
//        editor.clear();
//        editor.commit();
        initToolbars();
        activity = this;
        mNameView = (EditText) findViewById(R.id.register_name);
        mNumberView = (EditText) findViewById(R.id.register_phone_number);
        mConfirmPasswordView = (EditText) findViewById(R.id.confirm_password);
        mEmailView = (AutoCompleteTextView) findViewById(R.id.register_email);
        mShowPassword = (ImageView) findViewById(R.id.login_showpassword);
        mShowPassword1 = (ImageView) findViewById(R.id.login_showpassword1);
        mPasswordView = (EditText) findViewById(R.id.password);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        showProgress(false);

        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.login || id == EditorInfo.IME_NULL) {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });
        mConfirmPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.login || id == EditorInfo.IME_NULL) {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });

        mEmailSignInButton = (Button) findViewById(R.id.save_button);

        mEmailSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                disableFlag = false;
                checkPerm();


            }
        });

        mShowPassword.setVisibility(View.GONE);
        mShowPassword1.setVisibility(View.GONE);
        mShowPassword.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final boolean isOutsideView = event.getX() < 0 ||
                        event.getX() > v.getWidth() ||
                        event.getY() < 0 ||
                        event.getY() > v.getHeight();

                // change input type will reset cursor position, so we want to save it
                final int cursor = mPasswordView.getSelectionStart();

                if (isOutsideView || MotionEvent.ACTION_UP == event.getAction())
                    mPasswordView.setInputType(InputType.TYPE_CLASS_TEXT |
                            InputType.TYPE_TEXT_VARIATION_PASSWORD);
                else
                    mPasswordView.setInputType(InputType.TYPE_CLASS_TEXT |
                            InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);

                mPasswordView.setSelection(cursor);
                return true;
            }
        });
        mShowPassword1.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final boolean isOutsideView = event.getX() < 0 ||
                        event.getX() > v.getWidth() ||
                        event.getY() < 0 ||
                        event.getY() > v.getHeight();

                // change input type will reset cursor position, so we want to save it
                final int cursor = mConfirmPasswordView.getSelectionStart();

                if (isOutsideView || MotionEvent.ACTION_UP == event.getAction())
                    mConfirmPasswordView.setInputType(InputType.TYPE_CLASS_TEXT |
                            InputType.TYPE_TEXT_VARIATION_PASSWORD);
                else
                    mConfirmPasswordView.setInputType(InputType.TYPE_CLASS_TEXT |
                            InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);

                mConfirmPasswordView.setSelection(cursor);
                return true;
            }
        });

        mPasswordView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                mShowPassword.setVisibility(s.length() > 0 ? View.VISIBLE : View.GONE);
            }
        });
        mConfirmPasswordView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                mShowPassword1.setVisibility(s.length() > 0 ? View.VISIBLE : View.GONE);
            }
        });

        getEmailId();
        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, emails);
        adapter.setNotifyOnChange(true);
        mEmailView.setAdapter(adapter);

//        Thread.setDefaultUncaughtExceptionHandler(new MyExceptionHandler(this, Login.class));
    }

    private void initToolbars() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        toolbar.setPadding(0, 0, 0, 0);
        toolbar.setTitle(R.string.register);
    }

    private String getEmailId() {
        Pattern emailPattern = Patterns.EMAIL_ADDRESS; // API level 8+
        String possibleEmail = "";
        emails = new ArrayList<>();
        Account[] accounts = AccountManager.get(this).getAccounts();
        for (Account account : accounts) {
            emails.add(account.name);
            if (emailPattern.matcher(account.name).matches()) {
                possibleEmail = account.name;
            }
        }

        return possibleEmail;
    }

    private void checkPerm() {
        List<String> permissionsNeeded = new ArrayList<String>();
        final List<String> permissionsList = new ArrayList<String>();
        if (!addPermission(permissionsList, Manifest.permission.ACCESS_COARSE_LOCATION))
            permissionsNeeded.add("Location");
//        if (!addPermission(permissionsList, Manifest.permission.ACCESS_FINE_LOCATION))
//            permissionsNeeded.add("Fine location");
        if (!addPermission(permissionsList, Manifest.permission.READ_CONTACTS))
            permissionsNeeded.add("Contacts");
        if (!addPermission(permissionsList, Manifest.permission.CALL_PHONE))
            permissionsNeeded.add("Phone");
//        if (!addPermission(permissionsList, Manifest.permission.GET_ACCOUNTS))
//            permissionsNeeded.add("Get Accounts");

        final String[] PERMISSIONS_CONTACT = {Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.GET_ACCOUNTS, Manifest.permission.READ_CONTACTS, Manifest.permission.CALL_PHONE};

        Map<String, Integer> perms = new HashMap<String, Integer>();
        // Initial
        perms.put(Manifest.permission.ACCESS_COARSE_LOCATION, PackageManager.PERMISSION_GRANTED);
        perms.put(Manifest.permission.ACCESS_FINE_LOCATION, PackageManager.PERMISSION_GRANTED);
        perms.put(Manifest.permission.READ_CONTACTS, PackageManager.PERMISSION_GRANTED);
        perms.put(Manifest.permission.GET_ACCOUNTS, PackageManager.PERMISSION_GRANTED);
        perms.put(Manifest.permission.CALL_PHONE, PackageManager.PERMISSION_GRANTED);

        if (permissionsList.size() == 0) {
            attemptLogin();
        } else {
            if (permissionsList.size() > 0) {
                if (permissionsNeeded.size() > 0) {
                    // Need Rationale
                    String message = "You need to grant access to  " + permissionsNeeded.get(0);
                    for (int i = 1; i < permissionsNeeded.size(); i++)
                        message = message + ", " + permissionsNeeded.get(i);
                    showMessageOKCancel(message,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    ActivityCompat.requestPermissions(activity, permissionsList.toArray(new String[permissionsList.size()]),
                                            REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
                                }
                            });
                    return;
                }
                ActivityCompat.requestPermissions(activity, permissionsList.toArray(new String[permissionsList.size()]),
                        REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
                return;

            }
         /*   if (ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.CAMERA)) {
                showMessageOKCancel("You need to allow access to Camera",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                ActivityCompat.requestPermissions(activity, permissionsList.toArray(new String[permissionsList.size()]),
                                        REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
                            }
                        });

                return;
            } else {
                ActivityCompat.requestPermissions(activity, permissionsList.toArray(new String[permissionsList.size()]),
                        REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
                return;
            }*/
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS) {
            Map<String, Integer> perms = new HashMap<String, Integer>();
            // Initial
            perms.put(Manifest.permission.ACCESS_COARSE_LOCATION, PackageManager.PERMISSION_GRANTED);
            perms.put(Manifest.permission.ACCESS_FINE_LOCATION, PackageManager.PERMISSION_GRANTED);
            perms.put(Manifest.permission.READ_CONTACTS, PackageManager.PERMISSION_GRANTED);
            perms.put(Manifest.permission.CALL_PHONE, PackageManager.PERMISSION_GRANTED);
            perms.put(Manifest.permission.GET_ACCOUNTS, PackageManager.PERMISSION_GRANTED);

            // Fill with results
            for (int i = 0; i < permissions.length; i++)
                perms.put(permissions[i], grantResults[i]);
            // Check for ACCESS_FINE_LOCATION
            if (perms.get(Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED
                    && perms.get(Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED
                    && perms.get(Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED
                    && perms.get(Manifest.permission.GET_ACCOUNTS) == PackageManager.PERMISSION_GRANTED
                    && perms.get(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                attemptLogin();
            } else {
                Toast.makeText(getApplicationContext(), "All permissions has not been granted", Toast.LENGTH_LONG).show();

            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private boolean addPermission(List<String> permissionsList, String permission) {
        if (ContextCompat.checkSelfPermission(activity, permission) != PackageManager.PERMISSION_GRANTED) {
            permissionsList.add(permission);
            // Check for Rationale Option
            if (!ActivityCompat.shouldShowRequestPermissionRationale(activity, permission))
                return false;
        }
        return true;
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new android.support.v7.app.AlertDialog.Builder(RegisterActivity.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }


    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void attemptLogin() {

        // Reset errors.
//        mEmailView.setError(null);
        mPasswordView.setError(null);
        mConfirmPasswordView.setError(null);

        // Store values at the time of the login attempt.
        String email = mEmailView.getText().toString();
        String name = mNameView.getText().toString();
        String number = mNumberView.getText().toString();
        String password = mPasswordView.getText().toString();
        String confirmPassword = mPasswordView.getText().toString();


        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (!TextUtils.isEmpty(password) && !TextUtils.isEmpty(confirmPassword) && !isPasswordValid(password, confirmPassword)) {
            disableFlag = false;
            mPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        }

        // Check for a valid email address.
//        if (TextUtils.isEmpty(email)) {
//            disableFlag = false;
//            mEmailView.setError(getString(R.string.error_field_required));
//            focusView = mEmailView;
//            cancel = true;
//        } else if (!isEmailValid(email)) {
//            disableFlag = false;
//            mEmailView.setError(getString(R.string.error_invalid_email));
//            focusView = mEmailView;
//            cancel = true;
//        }

        if (cancel) {
            focusView.requestFocus();
        } else {
            if (!disableFlag) {
                showProgress(true);
                userLoginTask(name, email, password, number);

                mEmailView.setEnabled(false);
                mPasswordView.setEnabled(false);
                mConfirmPasswordView.setEnabled(false);
                mEmailSignInButton.setEnabled(false);
            }
        }
    }

    private boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        if (!email.isEmpty() && email.length() > 2)
            return true;
        else
            return false;
    }

    private boolean isPasswordValid(String password, String confirm) {
        //TODO: Replace this with your own logic
        if (password.length() > 2 && password.matches(confirm))
            return true;
        else
            return false;
    }

    private void showProgress(final boolean show) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
//                Animation rotation = AnimationUtils.loadAnimation(RegisterActivity.this, R.anim.rotate);
//                rotation.setRepeatCount(Animation.INFINITE);
                if (show) {
//                    mProgressView.startAnimation(rotation);
                    progressBar.setVisibility(View.VISIBLE);
                } else {
//                    mProgressView.clearAnimation();
                    progressBar.setVisibility(View.GONE);
                }
            }
        });
    }

    /**
     * Represents an asynchronous login/registration task used to authenticate
     * the user.
     */
    private void userLoginTask(final String mName, final String mEmail, final String mPassword, final String number) {

        Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
        startActivity(intent);

/*

        try {
            String url = ServiceURLManager.getInstance().getRegisterUrl();
//            Register register = new Register(mName,mEmail,number, mPassword);
//            register.setName(mName);
//            register.setPassword(mPassword);
//            register.setPhoneNumber(number);
//            register.setEmail(mEmail);
//            DataStorage.getInstance().setUsername(number);
//            DataStorage.getInstance().setPassword(mPassword);
//            DataStorage.getInstance().setStrEmail(mEmail);
//            DataStorage.getInstance().setStrNumber(number);

            ObjectMapper mapper = new ObjectMapper();
            String jsonObject = null;
//            try {
//                jsonObject = mapper.writeValueAsString(register);
//            } catch (IOException e) {
//                e.printStackTrace();
//            }

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonObject,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {

*/
/*                            ObjectMapper mapper = new ObjectMapper();
                            PassengerVO obj = null;
                            try {
                                obj = mapper.readValue(response.toString(), new TypeReference<PassengerVO>() {
                                });
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            showProgress(false);
                            if (obj != null) {
                                ModelManager.getInstance().setPassengerVO(obj);

                                DataStorage.getInstance().setPassword(mPassword);
                                DataStorage.getInstance().setUsername(number);
//
//                                Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
//                                startActivity(intent);
                                finish();
                            } else {
                                showError(getString(R.string.error_general), null);
                            }*//*


                        }
                    }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("response", error.toString());
                    showProgress(false);
                    mEmailView.setEnabled(true);
                    mPasswordView.setEnabled(true);
                    mConfirmPasswordView.setEnabled(true);
                    mEmailSignInButton.setEnabled(true);
                    if (error != null && error.networkResponse != null && error.networkResponse.statusCode == HttpURLConnection.HTTP_FORBIDDEN) {
                        showError(getString(R.string.error_incorrect_password), null);
                    } else {
                        showError(getString(R.string.error_general), null);
                    }
                }
            });
            request.setRetryPolicy(new DefaultRetryPolicy(
                    30000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

//            final JsonObjectRequest requestFinal = request;
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);//			}

        } catch (Exception e) {
            e.printStackTrace();

        }
*/

    }

    private void showError(final String message, final DialogInterface.OnClickListener okClicked) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                new AlertDialog.Builder(RegisterActivity.this, R.style.AppCompatAlertDialogStyle).setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle(R.string.app_name)
                        .setMessage(message)
                        .setPositiveButton(android.R.string.ok, okClicked)
                        .show();
            }
        });

    }
}

