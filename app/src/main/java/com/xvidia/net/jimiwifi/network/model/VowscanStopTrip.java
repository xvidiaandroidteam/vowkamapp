package com.xvidia.net.jimiwifi.network.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by vasu on 3/5/16.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class VowscanStopTrip {

    @JsonProperty("strId")
    String strId;
    @JsonProperty("toLat")
    Double toLat;
    @JsonProperty("toLong")
    Double toLong;


    public String getStrId() {
        return strId;
    }

    public void setStrId(String strId) {
        this.strId = strId;
    }

    public Double getToLat() {
        return toLat;
    }

    public void setToLat(Double toLat) {
        this.toLat = toLat;
    }

    public Double getToLong() {
        return toLong;
    }

    public void setToLong(Double toLong) {
        this.toLong = toLong;
    }



}
