package com.xvidia.net.jimiwifi.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.xvidia.net.jimiwifi.GenerateQRCodeActivity;
import com.xvidia.net.jimiwifi.MainActivity;
import com.xvidia.net.jimiwifi.MyApplication;
import com.xvidia.net.jimiwifi.R;
import com.xvidia.net.jimiwifi.ScannerActivity;
import com.xvidia.net.jimiwifi.UserProfileActivity;
import com.xvidia.net.jimiwifi.VowscanSubscriberActivity;
import com.xvidia.net.jimiwifi.WifiListActivity;
import com.xvidia.net.jimiwifi.network.ServiceURLManager;
import com.xvidia.net.jimiwifi.network.VolleySingleton;
import com.xvidia.net.jimiwifi.network.model.Car;
import com.xvidia.net.jimiwifi.network.model.CreateTrip;
import com.xvidia.net.jimiwifi.network.model.ModelManager;
import com.xvidia.net.jimiwifi.network.model.Trip;
import com.xvidia.net.jimiwifi.network.model.TripDetails;
import com.xvidia.net.jimiwifi.network.model.VowscanStopTrip;
import com.xvidia.net.jimiwifi.utils.AppConstants;

import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.Date;

/**
 * Created by vasu on 4/10/16.
 */
public class NavDrawerListAdapter extends RecyclerView.Adapter<NavDrawerListAdapter.ViewHolder>{

    private static final int TYPE_ITEM = 0;
    private String mNavTitles[];
    private static Context context;
    private int profile;        //int Resource for header view profile picture
    private int icon[];


    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView title;
        public ImageView imageView;
        public RelativeLayout itemLayout;

        public ViewHolder(View itemView, int ViewType, Context c) {
            super(itemView);
            context = c;
            itemView.setClickable(true);
            if(ViewType == TYPE_ITEM) {
                title = (TextView) itemView.findViewById(R.id.title);
                imageView = (ImageView) itemView.findViewById(R.id.icon);
                itemLayout = (RelativeLayout) itemView.findViewById(R.id.nav_item_layout);

            }
        }

    }

    public NavDrawerListAdapter(String Titles[],  int Icon[], Context passedContext){
        mNavTitles = Titles;
        icon = Icon;
        context = passedContext;
    }


    @Override
    public NavDrawerListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_drawer_row,parent,false);
        ViewHolder vhItem = new ViewHolder(v,viewType,context);
        return vhItem;
    }

    @Override
    public void onBindViewHolder(NavDrawerListAdapter.ViewHolder holder,final int position) {

        holder.title.setText(mNavTitles[position]); // Setting the Text with the array of our Titles
        holder.imageView.setImageResource(icon[position]);
        holder.itemLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.Drawer.closeDrawer(Gravity.LEFT);
                switch (position){
                    case 0:
                        Intent intent = new Intent(context, UserProfileActivity.class);
                        context.startActivity(intent);
                        break;
                    case 1:

                        break;
                    case 2:

                        break;
                    case 3:

                        break;
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return mNavTitles.length;
    }

}
