package com.xvidia.net.jimiwifi;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.speech.tts.TextToSpeech;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.zxing.Result;
import com.xvidia.net.jimiwifi.network.ServiceURLManager;
import com.xvidia.net.jimiwifi.network.VolleySingleton;
import com.xvidia.net.jimiwifi.network.model.CameraDetails;
import com.xvidia.net.jimiwifi.network.model.Car;
import com.xvidia.net.jimiwifi.network.model.CreateTrip;
import com.xvidia.net.jimiwifi.network.model.ModelManager;
import com.xvidia.net.jimiwifi.network.model.Trip;
import com.xvidia.net.jimiwifi.network.model.TripDetails;
import com.xvidia.net.jimiwifi.network.model.VowscanStopTrip;
import com.xvidia.net.jimiwifi.storage.DataStorage;

import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

/**
 * Created by vasu on 30/9/16.
 */

public class ScannerActivity extends AppCompatActivity implements ZXingScannerView.ResultHandler {
    private ZXingScannerView mScannerView;
    private String result;
    private Toolbar toolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Programmatically initialize the scanner view
        setContentView(R.layout.activity_scanner);
        initToolbars();
        callScanner();
    }

    public void callScanner() {
        mScannerView = new ZXingScannerView(this);
        setContentView(mScannerView);
        mScannerView.setKeepScreenOn(true);
        mScannerView.setResultHandler(this); // Register ourselves as a handler for scan results.
        mScannerView.startCamera();         // Start camera
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    private void initToolbars() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setPadding(0, 0, 0, 0);
        toolbar.setTitle(R.string.login);
    }


    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mScannerView != null)
            mScannerView.stopCamera();
    }

    @Override
    public void handleResult(Result rawResult) {
        // Do something with the result here

        Log.e("handler", rawResult.getText()); // Prints scan results
        Log.e("handler", rawResult.getBarcodeFormat().toString()); // Prints the scan format (qrcode)
        result = rawResult.getText();
//        result = "JH77JGRBS52SADSJ111A";

        if(result != null || result.equalsIgnoreCase("")) {
            callCreateTrip();
        }
    }

    private void callCreateTrip() {
        Date date = new Date();
        CreateTrip createTrip = new CreateTrip();
        createTrip.setCarRegNo(result);
        createTrip.setPassengerPhoneNumber("9882179479");
        createTrip.setStartTimeStamp(date.getTime() + "");
        createTrip.setStrId(date.getTime() + "");

        ObjectMapper mapper = new ObjectMapper();
        String jsonObject = null;
        try {
            jsonObject = mapper.writeValueAsString(createTrip);
        } catch (IOException e) {
            e.printStackTrace();
        }

        String subUrl = ServiceURLManager.getInstance().getVowscanCreateTripUrl();
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, subUrl, jsonObject, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                try {
                    ObjectMapper mapper = new ObjectMapper();
                    Trip obj = null;
                    try {
                        obj = mapper.readValue(response.toString(), new TypeReference<Trip>() {
                        });
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
//                    showProgress(false);

                    if (obj != null) {
                        ModelManager.getInstance().setTrip(obj);
                        String stringId = ModelManager.getInstance().getTrip().getStrId();
                        callTripVo(stringId);
//                        callSessionDeatils();
                    }

//                    Log.i(LOG_TAG, mApiKey);
//                    Log.i(LOG_TAG, mSessionSubscriberId);
//                    Log.i(LOG_TAG, mToken);

                } catch (Exception e) {

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error != null && error.networkResponse != null && error.networkResponse.statusCode == HttpURLConnection.HTTP_FORBIDDEN) {
                    showError(getString(R.string.error_general), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    });
                } else
                    if (error.networkResponse.statusCode == 409) {
                        callStopTrip();
                    } else {
                        showError("Login Error", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                            }
                        });
                    }
            }
        });
        VolleySingleton.getInstance(this).addToRequestQueue(request);
    }

    private void callTripVo(String strId) {
        String subUrl = ServiceURLManager.getInstance().getVowscanTripDetails(strId);

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, subUrl, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    ObjectMapper mapper = new ObjectMapper();
                    TripDetails obj = null;
                    try {
                        obj = mapper.readValue(response.toString(), new TypeReference<TripDetails>() {
                        });
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
//                    showProgress(false);

                    if (obj != null) {
                        ModelManager.getInstance().setTripDetails(obj);
                        callSessionDeatils();
                        Intent intent = new Intent(ScannerActivity.this, VowscanSubscriberActivity.class);
                        startActivity(intent);
                        finish();
                    }

//                    Log.i(LOG_TAG, mApiKey);
//                    Log.i(LOG_TAG, mSessionSubscriberId);
//                    Log.i(LOG_TAG, mToken);

                } catch (Exception e) {
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error != null && error.networkResponse != null && error.networkResponse.statusCode == HttpURLConnection.HTTP_FORBIDDEN) {
                    showError("callSessionDetails", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    });
                } else {
                    showError(getString(R.string.error_general), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    });
                }            }
        });
        VolleySingleton.getInstance(this).addToRequestQueue(request);


    }

    private void updateCameraList() {

        String url = ServiceURLManager.getInstance().updateCameraListUrl();

        CameraDetails cameraDetails = new CameraDetails();
        cameraDetails.setCameraName("");

        ObjectMapper mapper = new ObjectMapper();
        String jsonObject = null;
        try {
            jsonObject = mapper.writeValueAsString(cameraDetails);
        } catch (IOException e) {
            e.printStackTrace();
        }

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {



                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("response", error.toString());
                if (error != null && error.networkResponse != null && error.networkResponse.statusCode == HttpURLConnection.HTTP_FORBIDDEN) {

                } else {

                }
            }
        });
        request.setRetryPolicy(new DefaultRetryPolicy(
                30000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

//            final JsonObjectRequest requestFinal = request;
        VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);//			}
    }

    private void callStopTrip() {

        try {
            if (ModelManager.getInstance().getPassengerVO() == null) {
                showError(getString(R.string.error_general), null);
                return;
            }
            if (ModelManager.getInstance().getPassengerVO().getCurrentTrip() == null) {
                showError(getString(R.string.error_general), null);
                return;
            }
            String url = ServiceURLManager.getInstance().getStopTripUrl();
//        toLatitude = ModelManager.getInstance().getPassengerVO().getCurrentTrip().getToLat();
//        toLongitude = ModelManager.getInstance().getPassengerVO().getCurrentTrip().getToLong();
            VowscanStopTrip latLongObject = new VowscanStopTrip();
            latLongObject.setStrId(ModelManager.getInstance().getPassengerVO().getCurrentTrip().getStrId());
//        latLongObject.setToLat(toLatitude);
//        latLongObject.setToLong(toLongitude);

            ObjectMapper mapper = new ObjectMapper();
            String jsonObject = null;
            try {
                jsonObject = mapper.writeValueAsString(latLongObject);
            } catch (IOException e) {
                e.printStackTrace();
            }

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonObject,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {

                            ObjectMapper mapper = new ObjectMapper();
                            Trip obj = null;
                            try {
                                obj = mapper.readValue(response.toString(), new TypeReference<Trip>() {
                                });
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
//                        showProgress(false);
                            if (obj != null) {
                                callCreateTrip();
//                            showError("Trip has been stopped. Starting new trip...", null);

                            } else {
                                showError(getString(R.string.error_general), null);
                            }

                        }
                    }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
//                    Log.e("response", error.toString());
//                showProgress(false);
//                    mEmailView.setEnabled(true);
//                    mPasswordView.setEnabled(true);
//                    mConfirmPasswordView.setEnabled(true);
//                    mEmailSignInButton.setEnabled(true);
                    if (error != null && error.networkResponse != null && error.networkResponse.statusCode == HttpURLConnection.HTTP_FORBIDDEN) {
                        showError(getString(R.string.error_general), null);
                    } else {
                        showError(getString(R.string.error_general), null);
                    }
                }
            });
            request.setRetryPolicy(new DefaultRetryPolicy(
                    30000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

//            final JsonObjectRequest requestFinal = request;
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);//			}

        } catch (Exception e) {
            e.printStackTrace();

        }

    }

    private void callSessionDeatils() {

        if (ModelManager.getInstance().getPassengerVO() == null) {
            showError(getString(R.string.error_general), null);
            return;
        }
        String regNo = ModelManager.getInstance().getTripDetails().getCar().getRegistrationNumber();
        String subUrl = ServiceURLManager.getInstance().getVowscanLiveSubscriberTokenUrl(regNo);
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, subUrl, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    ObjectMapper mapper = new ObjectMapper();
                    Car obj = null;
                    try {
                        obj = mapper.readValue(response.toString(), new TypeReference<Car>() {
                        });
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
//                    showProgress(false);

                    if (obj != null) {
                        ModelManager.getInstance().setCar(obj);
                        Intent intent = new Intent(ScannerActivity.this, VowscanSubscriberActivity.class);
                        startActivity(intent);
                        finish();
                    }

//                    Log.i(LOG_TAG, mApiKey);
//                    Log.i(LOG_TAG, mSessionSubscriberId);
//                    Log.i(LOG_TAG, mToken);

                } catch (Exception e) {
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error != null && error.networkResponse != null && error.networkResponse.statusCode == HttpURLConnection.HTTP_FORBIDDEN) {
                    showError("callSessionDetails", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    });
                } else {
                    showError(getString(R.string.error_general), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    });
                }            }
        });
        VolleySingleton.getInstance(this).addToRequestQueue(request);
    }

    private void showError(final String message, final DialogInterface.OnClickListener okClicked) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {
                    new AlertDialog.Builder(ScannerActivity.this, R.style.AppCompatAlertDialogStyle).setIcon(android.R.drawable.ic_dialog_alert)
                            .setTitle(R.string.app_name)
                            .setMessage(message)
                            .setPositiveButton(android.R.string.ok, okClicked)
                            .show();
                }catch (WindowManager.BadTokenException e){}
            }
        });
    }
}
