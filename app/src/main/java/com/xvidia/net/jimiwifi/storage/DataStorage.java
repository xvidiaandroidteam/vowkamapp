package com.xvidia.net.jimiwifi.storage;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.xvidia.net.jimiwifi.MyApplication;


public class DataStorage {
	public static String MY_PREFERENCES = "SETTINGPREFERENCE";
	public static SharedPreferences sharedpreferences;
	public static final String STR_REMEMBER = "REMEBERME";
	public static final String STR_RESPONSE = "RESPONSE";
	public static final String STR_USERNAME = "USERNAME";
	public static final String STR_TOKEN = "STR_TOKEN";
	public static final String STR_PASSWORD = "PASSWORD";
	public static final String STR_EMAIL = "EMAIL";
	public static final String STR_NUMBER = "NUMBER";
	public static final String STR_CONTACT_NAME = "CONTACT_NAME";
	public static final String STR_CONTACT_NUMBER = "CONTACT_NUMBER";
	private static DataStorage instance = null;


	public static DataStorage getInstance() {
		if (instance == null) {
			instance = new DataStorage();
		}
		return instance;
	}

	private SharedPreferences getSharedPreference() {
		sharedpreferences = MyApplication.getAppContext().getSharedPreferences(MY_PREFERENCES,
				Context.MODE_PRIVATE);
		return sharedpreferences;
	}

	public String getUsername() {
		try {
			return getSharedPreference().getString(STR_USERNAME, "");
		} catch (Exception e) {
		}
		return "";
	}

	public void setUsername(String value) {
		try {
			Editor editor = getSharedPreference().edit();
			editor.putString(STR_USERNAME, value);
			editor.apply();
		} catch (Exception e) {
			// retVal = STR_UNKNOWN;
		}
	}

	public String getAccessToken() {
		try {
			return getSharedPreference().getString(STR_TOKEN, "");
		} catch (Exception e) {
		}
		return "";
	}

	public void setAccessToken(String value) {
		try {
			Editor editor = getSharedPreference().edit();
			editor.putString(STR_TOKEN, value);
			editor.apply();
		} catch (Exception e) {
			// retVal = STR_UNKNOWN;
		}
	}

	public String getPassword() {
		try {
			return getSharedPreference().getString(STR_PASSWORD, "");
		} catch (Exception e) {
		}
		return "";
	}

	public void setPassword(String value) {
		try {
			Editor editor = getSharedPreference().edit();
			editor.putString(STR_PASSWORD, value);
			editor.apply();
		} catch (Exception e) {
			// retVal = STR_UNKNOWN;
		}
	}
	public String getStrEmail() {
		try {
			return getSharedPreference().getString(STR_EMAIL, "");
		} catch (Exception e) {
		}
		return "";
	}

	public void setStrEmail(String value) {
		try {
			Editor editor = getSharedPreference().edit();
			editor.putString(STR_USERNAME, value);
			editor.apply();
		} catch (Exception e) {
			// retVal = STR_UNKNOWN;
		}
	}

	public String getStrNumber() {
		try {
			return getSharedPreference().getString(STR_NUMBER, "");
		} catch (Exception e) {
		}
		return "";
	}

	public void setStrNumber(String value) {
		try {
			Editor editor = getSharedPreference().edit();
			editor.putString(STR_PASSWORD, value);
			editor.apply();
		} catch (Exception e) {
			// retVal = STR_UNKNOWN;
		}
	}

	public boolean getRemember() {
		try {
			return getSharedPreference().getBoolean(STR_REMEMBER, false);
		} catch (Exception e) {
		}
		return false;
	}

	public void setRemeber(boolean value) {
		try {
			Editor editor = getSharedPreference().edit();
			editor.putBoolean(STR_REMEMBER, value);
			editor.apply();
		} catch (Exception e) {
			// retVal = STR_UNKNOWN;
		}
	}

	public String getResponse() {
		try {
			return getSharedPreference().getString(STR_RESPONSE, "");
		} catch (Exception e) {

		}
		return "";
	}

	public void setResponse(String value) {
		try {
			Editor editor = getSharedPreference().edit();
			editor.putString(STR_RESPONSE, value);
			editor.apply();
		} catch (Exception e) {
			// retVal = STR_UNKNOWN;
		}
	}
	public void setStrContactName(String value) {
		try {

			Editor editor = getSharedPreference().edit();
			editor.putString(STR_RESPONSE, value);
			editor.apply();
		} catch (Exception e) {
			// retVal = STR_UNKNOWN;
		}
	}
	public void setStrContactNumber(String value) {
		try {
			Editor editor = getSharedPreference().edit();
			editor.putString(STR_RESPONSE, value);
			editor.apply();
		} catch (Exception e) {
			// retVal = STR_UNKNOWN;
		}
	}
}