package com.xvidia.net.jimiwifi.network;

import com.xvidia.net.jimiwifi.MyApplication;
import com.xvidia.net.jimiwifi.R;

/**
 * Created by vasu on 4/10/16.
 */
public class ServiceURLManager {

    private static ServiceURLManager instance = null;

    private ServiceURLManager() {

    }

    public static ServiceURLManager getInstance() {
        if (instance == null) {
            instance = new ServiceURLManager();
        }
        return instance;
    }

    final String base_URL = MyApplication.getAppContext().getResources().getString(R.string.base_url);

    public String getRegisterUrl() {
        String url = "http://52.87.117.228:9090/" + "";
        url = url.replace("+", "%2B");
        url = url.replace(" ","%20");
        return url;
    }

    public String getLoginUrl() {
        String url = "http://52.87.117.228:9090/" + "passenger/login";
        url = url.replace("+", "%2B");
        url = url.replace(" ","%20");
        return url;
    }

    public String getCameraListUrl() {
        String url = "http://52.87.117.228:9090/" + "";
        url = url.replace("+", "%2B");
        url = url.replace(" ","%20");
        return url;
    }

    public String updateCameraListUrl() {
        String url = "http://52.87.117.228:9090/" + "";
        url = url.replace("+", "%2B");
        url = url.replace(" ","%20");
        return url;
    }

    public String getVowscanCreateTripUrl() {
        String url = "http://52.87.117.228:9090/"+ "createTrip";
        url = url.replace("+", "%2B");
        url = url.replace(" ","%20");
        return url;
    }

    public String getVowscanTripDetails(String strId) {
        String url = "http://52.87.117.228:9090/" +"/tripDetails/"+ strId;
        url = url.replace("+","%2B");
        url = url.replace(" ","%20");
        return url;
    }

    public String getStopTripUrl() {
        String url = "http://52.87.117.228:9090/" + "stopTrip";
        url = url.replace("+","%2B");
        url = url.replace(" ","%20");
        return url;
    }

    public String getVowscanLiveSubscriberTokenUrl(String regNo) {
        String url = "http://52.87.117.228:9090/" + "getCarByRegNo/"+regNo;
        url = url.replace("+", "%2B");
        url = url.replace(" ","%20");
        return url;
    }
}
