package com.xvidia.net.jimiwifi;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.xvidia.net.jimiwifi.helper.CircularImageView;

/**
 * Created by vasu on 7/3/17.
 */

public class UserProfileActivity extends AppCompatActivity {

    private CircularImageView userDp;
    private TextView email, changePassword;
    private RadioButton wifiRadio, wifiCellularRadio, cellularRadio;
    private CollapsingToolbarLayout collapsingToolbar;
    private Toolbar mToolbar;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);
        bindActivity();
        collapsingToolbar =
                (CollapsingToolbarLayout) findViewById(R.id.collapse_toolbar);
        collapsingToolbar.setCollapsedTitleTextAppearance(R.style.collapsedappbar);
        collapsingToolbar.setExpandedTitleTextAppearance(R.style.expandedappbar);
        collapsingToolbar.setTitle("");

        userDp = (CircularImageView) findViewById(R.id.imageView);
        email = (TextView) findViewById(R.id.email);
        changePassword = (TextView) findViewById(R.id.password);
        wifiRadio = (RadioButton) findViewById(R.id.wifi);
        wifiCellularRadio = (RadioButton) findViewById(R.id.wifiCellular);
        cellularRadio = (RadioButton) findViewById(R.id.cellular);
        changePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog openDialog = new Dialog(UserProfileActivity.this);
                openDialog.setContentView(R.layout.custom_dialog);
                openDialog.setTitle("Custom Dialog Box");
                EditText oldPass = (EditText) openDialog.findViewById(R.id.oldPassword);
                EditText newPass = (EditText) openDialog.findViewById(R.id.newPassword);
                EditText cnfrmPass = (EditText) openDialog.findViewById(R.id.confirm_password);
                Button dialogSubmitButton = (Button) openDialog.findViewById(R.id.submit);

                dialogSubmitButton.setOnClickListener(new View.OnClickListener(){
                    @Override
                    public void onClick(View v) {
                        // TODO Auto-generated method stub
                        openDialog.dismiss();
                    }
                });
                openDialog.show();
            }
        });
        wifiRadio.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(cellularRadio.isChecked())
                    cellularRadio.setChecked(false);
                if(wifiCellularRadio.isChecked())
                    wifiCellularRadio.setChecked(false);
                wifiRadio.setChecked(isChecked);
            }
        });
        wifiCellularRadio.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(cellularRadio.isChecked())
                    cellularRadio.setChecked(false);
                if(wifiRadio.isChecked())
                    wifiRadio.setChecked(false);
                wifiCellularRadio.setChecked(isChecked);
            }
        });
        cellularRadio.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(wifiCellularRadio.isChecked())
                    wifiCellularRadio.setChecked(false);
                if(wifiRadio.isChecked())
                    wifiRadio.setChecked(false);
                cellularRadio.setChecked(isChecked);
            }
        });
    }

    private void bindActivity() {

        mToolbar = (Toolbar) findViewById(R.id.MyToolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mToolbar.setPadding(0, 0, 0, 0);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


//        getVideoUrls();
    }
}
