package com.xvidia.net.jimiwifi;

import android.app.Activity;
import android.content.BroadcastReceiver;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;

import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.Bundle;

import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;

import java.util.List;

public class WifiListActivity extends AppCompatActivity {
    ListView lv;
    WifiManager wifi;
    String wifis[];
    WifiScanReceiver wifiReciever;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wifi_list);
        initToolbars();
        lv = (ListView) findViewById(R.id.wifiList);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,final int position, long id) {

                final AlertDialog.Builder alert = new AlertDialog.Builder(WifiListActivity.this);
                final EditText edittext = new EditText(WifiListActivity.this);
                alert.setMessage("Enter password");
                alert.setTitle("PASSWORD");

                alert.setView(edittext);

                alert.setPositiveButton("Done", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        //What ever you want to do with the value
                        String password = edittext.getText().toString();
                        String wifiName = wifis[position].toString();
                        Intent intent = new Intent(WifiListActivity.this, GenerateQRCodeActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString("WIFINAME", wifiName);
                        bundle.putString("PASSWORD", password);
                        intent.putExtras(bundle);
                        startActivity(intent);
                        finish();
                    }
                });

                alert.show();
            }
        });

        wifi = (WifiManager) getSystemService(Context.WIFI_SERVICE);
        wifiReciever = new WifiScanReceiver();
        wifi.startScan();
    }

    private void initToolbars() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setPadding(0, 0, 0, 0);
        toolbar.setTitle(R.string.login);
    }

    protected void onPause() {
        unregisterReceiver(wifiReciever);
        super.onPause();
    }

    protected void onResume() {
        registerReceiver(wifiReciever, new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));
        super.onResume();
    }

    private class WifiScanReceiver extends BroadcastReceiver {
        public void onReceive(Context c, Intent intent) {
            List<ScanResult> wifiScanList = wifi.getScanResults();
            wifis = new String[wifiScanList.size()];

            for (int i = 0; i < wifiScanList.size(); i++) {
                wifis[i] = ((wifiScanList.get(i)).SSID.toString());
            }

            lv.setAdapter(new ArrayAdapter<String>(getApplicationContext(), R.layout.wifi_list_items, wifis));
        }
    }
}