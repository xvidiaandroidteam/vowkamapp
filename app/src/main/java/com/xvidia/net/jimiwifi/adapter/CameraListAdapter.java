package com.xvidia.net.jimiwifi.adapter;

/**
 * Created by vasu on 4/10/16.
 */

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.xvidia.net.jimiwifi.MainActivity;
import com.xvidia.net.jimiwifi.MyApplication;
import com.xvidia.net.jimiwifi.R;
import com.xvidia.net.jimiwifi.ShareManagementActivity;
import com.xvidia.net.jimiwifi.UserProfileActivity;
import com.xvidia.net.jimiwifi.VowscanSubscriberActivity;
import com.xvidia.net.jimiwifi.helper.CustomFontTextView;
import com.xvidia.net.jimiwifi.network.ServiceURLManager;
import com.xvidia.net.jimiwifi.network.VolleySingleton;
import com.xvidia.net.jimiwifi.network.model.CameraDetails;
import com.xvidia.net.jimiwifi.network.model.Car;
import com.xvidia.net.jimiwifi.network.model.CreateTrip;
import com.xvidia.net.jimiwifi.network.model.ModelManager;
import com.xvidia.net.jimiwifi.network.model.Trip;
import com.xvidia.net.jimiwifi.network.model.TripDetails;
import com.xvidia.net.jimiwifi.network.model.VowscanStopTrip;

import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.Date;
import java.util.List;

public class CameraListAdapter extends RecyclerView.Adapter<CameraListAdapter.MyViewHolder> {

    private List<CameraDetails> moviesList;
    private Context context;
    private View view;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView cameraName, status;
        public ImageView imageView;
        public CustomFontTextView share, refresh, cameraDelete;

        public MyViewHolder(View view) {
            super(view);
            cameraName = (TextView) view.findViewById(R.id.cameraName);
            cameraDelete = (CustomFontTextView) view.findViewById(R.id.delete);
            share = (CustomFontTextView) view.findViewById(R.id.share);
            refresh = (CustomFontTextView) view.findViewById(R.id.refresh);
            status = (TextView) view.findViewById(R.id.status);
            imageView = (ImageView) view.findViewById(R.id.imageView);
        }
    }


    public CameraListAdapter(List<CameraDetails> moviesList, Context context) {
        this.moviesList = moviesList;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_camera_list_layout, parent, false);
//        view = itemView;
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        CameraDetails movie = moviesList.get(position);
        holder.cameraName.setText(movie.getCameraName());
        if(movie.getStatus().equalsIgnoreCase("ON")){
            holder.status.setTextColor(context.getResources().getColor(R.color.green));
        }else{
            holder.status.setTextColor(context.getResources().getColor(R.color.red));
        }
        holder.status.setText(movie.getStatus());
        holder.share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ShareManagementActivity.class);
                context.startActivity(intent);
            }
        });
        holder.refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, "Refresh", Toast.LENGTH_LONG).show();
            }
        });
        holder.cameraDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, "Delete", Toast.LENGTH_LONG).show();
            }
        });
        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, "Image", Toast.LENGTH_LONG).show();
            }
        });

//        holder.location.setText(movie.getLocation());

        /*view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (position){
                    case 0:
                        callCreateTrip();
                        break;

                    case 1:

                        break;

                    case 2:



                        break;
                }
            }
        });*/
    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }

    private void callCreateTrip() {
        Date date = new Date();
        CreateTrip createTrip = new CreateTrip();
        createTrip.setCarRegNo("JH77JGRBS52SADSJ111A");
        createTrip.setPassengerPhoneNumber("9882179479");
        createTrip.setStartTimeStamp(date.getTime() + "");
        createTrip.setStrId(date.getTime() + "");

        ObjectMapper mapper = new ObjectMapper();
        String jsonObject = null;
        try {
            jsonObject = mapper.writeValueAsString(createTrip);
        } catch (IOException e) {
            e.printStackTrace();
        }

        String subUrl = ServiceURLManager.getInstance().getVowscanCreateTripUrl();
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, subUrl, jsonObject, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                try {
                    ObjectMapper mapper = new ObjectMapper();
                    Trip obj = null;
                    try {
                        obj = mapper.readValue(response.toString(), new TypeReference<Trip>() {
                        });
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
//                    showProgress(false);

                    if (obj != null) {
                        ModelManager.getInstance().setTrip(obj);
                        String stringId = ModelManager.getInstance().getTrip().getStrId();
                        callTripVo(stringId);
//                        callSessionDeatils();
                    }

//                    Log.i(LOG_TAG, mApiKey);
//                    Log.i(LOG_TAG, mSessionSubscriberId);
//                    Log.i(LOG_TAG, mToken);

                } catch (Exception e) {

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error != null && error.networkResponse != null && error.networkResponse.statusCode == HttpURLConnection.HTTP_FORBIDDEN) {
                    showError(context.getString(R.string.error_general), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            ((MainActivity) context).finish();
                        }
                    });
                } else {
                    if (error.networkResponse.statusCode == 409) {
                        callStopTrip();
                    } else {
                        showError("CreateTrip", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                ((MainActivity) context).finish();
                            }
                        });
                    }

                }
            }
        });
        VolleySingleton.getInstance(context).addToRequestQueue(request);
    }

    private void callTripVo(String strId) {
        String subUrl = ServiceURLManager.getInstance().getVowscanTripDetails(strId);

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, subUrl, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    ObjectMapper mapper = new ObjectMapper();
                    TripDetails obj = null;
                    try {
                        obj = mapper.readValue(response.toString(), new TypeReference<TripDetails>() {
                        });
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
//                    showProgress(false);

                    if (obj != null) {
                        ModelManager.getInstance().setTripDetails(obj);
                        callSessionDeatils();
                    }

//                    Log.i(LOG_TAG, mApiKey);
//                    Log.i(LOG_TAG, mSessionSubscriberId);
//                    Log.i(LOG_TAG, mToken);

                } catch (Exception e) {
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error != null && error.networkResponse != null && error.networkResponse.statusCode == HttpURLConnection.HTTP_FORBIDDEN) {
                    showError("callSessionDetails", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            ((MainActivity) context).finish();
                        }
                    });
                } else {
                    showError(context.getString(R.string.error_general), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            ((MainActivity) context).finish();
                        }
                    });
                }            }
        });
        VolleySingleton.getInstance(context).addToRequestQueue(request);


    }

    private void callStopTrip() {

        try {
            if (ModelManager.getInstance().getPassengerVO() == null) {
                showError(context.getString(R.string.error_general), null);
                return;
            }
            if (ModelManager.getInstance().getPassengerVO().getCurrentTrip() == null) {
                showError(context.getString(R.string.error_general), null);
                return;
            }
            String url = ServiceURLManager.getInstance().getStopTripUrl();
//        toLatitude = ModelManager.getInstance().getPassengerVO().getCurrentTrip().getToLat();
//        toLongitude = ModelManager.getInstance().getPassengerVO().getCurrentTrip().getToLong();
            VowscanStopTrip latLongObject = new VowscanStopTrip();
            latLongObject.setStrId(ModelManager.getInstance().getPassengerVO().getCurrentTrip().getStrId());
//        latLongObject.setToLat(toLatitude);
//        latLongObject.setToLong(toLongitude);

            ObjectMapper mapper = new ObjectMapper();
            String jsonObject = null;
            try {
                jsonObject = mapper.writeValueAsString(latLongObject);
            } catch (IOException e) {
                e.printStackTrace();
            }

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonObject,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {

                            ObjectMapper mapper = new ObjectMapper();
                            Trip obj = null;
                            try {
                                obj = mapper.readValue(response.toString(), new TypeReference<Trip>() {
                                });
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
//                        showProgress(false);
                            if (obj != null) {
                                callCreateTrip();
//                            showError("Trip has been stopped. Starting new trip...", null);

                            } else {
                                showError(context.getString(R.string.error_general), null);
                            }

                        }
                    }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
//                    Log.e("response", error.toString());
//                showProgress(false);
//                    mEmailView.setEnabled(true);
//                    mPasswordView.setEnabled(true);
//                    mConfirmPasswordView.setEnabled(true);
//                    mEmailSignInButton.setEnabled(true);
                    if (error != null && error.networkResponse != null && error.networkResponse.statusCode == HttpURLConnection.HTTP_FORBIDDEN) {
                        showError(context.getString(R.string.error_general), null);
                    } else {
                        showError(context.getString(R.string.error_general), null);
                    }
                }
            });
            request.setRetryPolicy(new DefaultRetryPolicy(
                    30000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

//            final JsonObjectRequest requestFinal = request;
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);//			}

        } catch (Exception e) {
            e.printStackTrace();

        }

    }

    private void callSessionDeatils() {

        if (ModelManager.getInstance().getPassengerVO() == null) {
            showError(context.getString(R.string.error_general), null);
            return;
        }
        String regNo = ModelManager.getInstance().getTripDetails().getCar().getRegistrationNumber();
        String subUrl = ServiceURLManager.getInstance().getVowscanLiveSubscriberTokenUrl(regNo);
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, subUrl, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    ObjectMapper mapper = new ObjectMapper();
                    Car obj = null;
                    try {
                        obj = mapper.readValue(response.toString(), new TypeReference<Car>() {
                        });
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
//                    showProgress(false);

                    if (obj != null) {
                        ModelManager.getInstance().setCar(obj);
                        Intent intent = new Intent(context, VowscanSubscriberActivity.class);
                        context.startActivity(intent);
                    }

//                    Log.i(LOG_TAG, mApiKey);
//                    Log.i(LOG_TAG, mSessionSubscriberId);
//                    Log.i(LOG_TAG, mToken);

                } catch (Exception e) {
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error != null && error.networkResponse != null && error.networkResponse.statusCode == HttpURLConnection.HTTP_FORBIDDEN) {
                    showError("callSessionDetails", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            ((MainActivity) context).finish();
                        }
                    });
                } else {
                    showError(context.getString(R.string.error_general), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            ((MainActivity) context).finish();
                        }
                    });
                }            }
        });
        VolleySingleton.getInstance(context).addToRequestQueue(request);
    }

    private void showError(final String message, final DialogInterface.OnClickListener okClicked) {
        ((MainActivity) context).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {
                    new AlertDialog.Builder(context, R.style.AppCompatAlertDialogStyle).setIcon(android.R.drawable.ic_dialog_alert)
                            .setTitle(R.string.app_name)
                            .setMessage(message)
                            .setPositiveButton(android.R.string.ok, okClicked)
                            .show();
                }catch (WindowManager.BadTokenException e){}
            }
        });
    }
}
