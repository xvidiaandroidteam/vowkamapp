package com.xvidia.net.jimiwifi;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.PersistableBundle;
import android.provider.MediaStore;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Cache;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonObjectRequest;
import com.dropbox.client2.DropboxAPI;
import com.dropbox.client2.android.AndroidAuthSession;
import com.dropbox.client2.session.AppKeyPair;
import com.xvidia.net.jimiwifi.adapter.CameraListAdapter;
import com.xvidia.net.jimiwifi.adapter.NavDrawerListAdapter;
import com.xvidia.net.jimiwifi.helper.MyCustomLayoutManager;
import com.xvidia.net.jimiwifi.network.ServiceURLManager;
import com.xvidia.net.jimiwifi.network.VolleySingleton;
import com.xvidia.net.jimiwifi.network.model.CameraDetails;
import com.xvidia.net.jimiwifi.storage.DataStorage;
import com.xvidia.net.jimiwifi.utils.AppConstants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by vasu on 4/10/16.
 */

public class MainActivity extends AppCompatActivity {

    private static final int IMAGE_REQUEST_CODE = 100 ;
    private static final String TAG = MainActivity.class.getSimpleName() ;
    RecyclerView mRecyclerView;
    private ImageView groupMenu;
    // Declaring RecyclerView
    RecyclerView.LayoutManager mLayoutManager;            // Declaring Layout Manager as a linear layout manager
    public static DrawerLayout Drawer;
    public NavDrawerListAdapter mAdapter;
    private Toolbar toolbar;                              // Declaring the Toolbar Object
    private ActionBarDrawerToggle mDrawerToggle;
    int[] ICON = new int[]{
            R.drawable.plus,
            R.drawable.plus,
            R.drawable.plus,
            R.drawable.plus
    };
    String TITLES[] = {
            MyApplication.getAppContext().getResources().getString(R.string.user),
            MyApplication.getAppContext().getResources().getString(R.string.about),
            MyApplication.getAppContext().getResources().getString(R.string.logout),
            MyApplication.getAppContext().getResources().getString(R.string.dropbox)
    };
    private DropboxAPI<AndroidAuthSession> mApi;
    private String ACCESS_TOKEN;
    private String mCameraFileName;
    private final String PHOTO_DIR = "/Photos/";

    private List<CameraDetails> cameraList = new ArrayList<>();
    private RecyclerView cameraListrecyclerView;
    private CameraListAdapter cameraListAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initToolbars();
        mRecyclerView = (RecyclerView) findViewById(R.id.RecyclerView); // Assigning the RecyclerView Object to the xml View

        mRecyclerView.setHasFixedSize(true);                            // Letting the system know that the list objects are of fixed size

        mAdapter = new NavDrawerListAdapter(TITLES, ICON, MainActivity.this);
        mRecyclerView.setAdapter(mAdapter);                              // Setting the adapter to RecyclerView

        mLayoutManager = new MyCustomLayoutManager(this, true);                 // Creating a layout Manager

        mRecyclerView.setLayoutManager(mLayoutManager);
        Drawer = (DrawerLayout) findViewById(R.id.DrawerLayout);        // Drawer object Assigned to the view

        mDrawerToggle = new ActionBarDrawerToggle(this, Drawer, toolbar,
                R.string.drawer_open, R.string.drawer_close) {
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                // getSupportActionBar().setTitle("Navigation!");
                invalidateOptionsMenu();
            }

            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
//                getSupportActionBar().setTitle(mActivityTitle);
                invalidateOptionsMenu();
            }
        };

        mDrawerToggle.setDrawerIndicatorEnabled(true);
        Drawable drawable = ResourcesCompat.getDrawable(getResources(), R.drawable.hemburgar_icon, getTheme());
        mDrawerToggle.setHomeAsUpIndicator(drawable);
        mDrawerToggle.setToolbarNavigationClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Drawer.isDrawerVisible(GravityCompat.START)) {
                    Drawer.closeDrawer(GravityCompat.START);
                } else {
                    Drawer.openDrawer(GravityCompat.START);
                }
            }
        });
        Drawer.addDrawerListener(mDrawerToggle); // Drawer Listener set to the Drawer toggle
        mDrawerToggle.syncState();               // Finally we set the drawer toggle sync State

        cameraListrecyclerView = (RecyclerView) findViewById(R.id.cameraList);
        CameraDetails cameraDetails = new CameraDetails("Drawing room", "OFF");
        cameraList.add(cameraDetails);
        CameraDetails cameraDetails2 = new CameraDetails("Kitchen", "ON");
        cameraList.add(cameraDetails2);
        CameraDetails cameraDetails3 = new CameraDetails("Bedroom", "ON");
        cameraList.add(cameraDetails3);
        cameraListAdapter = new CameraListAdapter(cameraList, MainActivity.this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        cameraListrecyclerView.setLayoutManager(mLayoutManager);
        cameraListrecyclerView.setItemAnimator(new DefaultItemAnimator());
        cameraListrecyclerView.setAdapter(cameraListAdapter);
//        initialize_session();
//        callCameraList();
    }

    public void addImage(View v) {
        Intent intent = new Intent();
        // Picture from camera
        intent.setAction(MediaStore.ACTION_IMAGE_CAPTURE);

        // This is not the right way to do this, but for some reason,
        // having
        // it store it in
        // MediaStore.Images.Media.EXTERNAL_CONTENT_URI isn't working
        // right.

        Date date = new Date();
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd-kk-mm-ss",
                Locale.US);

        String newPicFile = df.format(date) + ".jpg";
        String outPath = new File(Environment
                .getExternalStorageDirectory(), newPicFile).getPath();
        File outFile = new File(outPath);

        mCameraFileName = outFile.toString();
        Uri outuri = Uri.fromFile(outFile);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, outuri);
        Log.i(TAG, "Importing New Picture: " + mCameraFileName);
        try {
            startActivityForResult(intent, IMAGE_REQUEST_CODE);
        } catch (ActivityNotFoundException e) {
            showToast("There doesn't seem to be a camera.");
        }
    }

    private void showToast(String msg) {
        Toast error = Toast.makeText(this, msg, Toast.LENGTH_LONG);
        error.show();
    }

    protected void initialize_session(){

        // store app key and secret key
        AppKeyPair appKeys = new AppKeyPair(AppConstants.DROPBOX_APP_KEY, AppConstants.DROPBOX_APP_SECRET);
        AndroidAuthSession session = new AndroidAuthSession(appKeys);

        // Pass app key pair to the new DropboxAPI object.
        mApi = new DropboxAPI<AndroidAuthSession>(session);

        // MyActivity below should be your activity class name
        // start authentication.
        if(DataStorage.getInstance().getAccessToken().equalsIgnoreCase(""))
        mApi.getSession().startOAuth2Authentication(MainActivity.this);
        else mApi.getSession().setOAuth2AccessToken(DataStorage.getInstance().getAccessToken());
    }

    private void callCameraList() {


        cameraListAdapter.notifyDataSetChanged();


     /*   String url = ServiceURLManager.getInstance().getCameraListUrl();

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                url, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("RECENT_ERROR", "Error: " + error.getMessage());
                if (error != null && error.networkResponse != null && error.networkResponse.statusCode == HttpURLConnection.HTTP_NOT_FOUND) {

                }
            }
        }) {

            @Override
            protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
                try {
                    Cache.Entry cacheEntry = HttpHeaderParser.parseCacheHeaders(response);
                    if (cacheEntry == null) {
                        cacheEntry = new Cache.Entry();
                    }                    long now = System.currentTimeMillis();
                    final long softExpire = now + AppConstants.cacheHitButRefreshed;
                    final long ttl = now + AppConstants.cacheExpired;
                    cacheEntry.data = response.data;
                    cacheEntry.softTtl = softExpire;
                    cacheEntry.ttl = ttl;
                    String headerValue;
                    headerValue = response.headers.get("Date");
                    if (headerValue != null) {
                        cacheEntry.serverDate = HttpHeaderParser.parseDateAsEpoch(headerValue);
                    }
                    headerValue = response.headers.get("Last-Modified");
                    if (headerValue != null) {
                        cacheEntry.lastModified = HttpHeaderParser.parseDateAsEpoch(headerValue);
                    }
                    cacheEntry.responseHeaders = response.headers;
                    final String jsonString = new String(response.data,
                            HttpHeaderParser.parseCharset(response.headers));
                    return Response.success(new JSONObject(jsonString), cacheEntry);
                } catch (UnsupportedEncodingException e) {
                    return Response.error(new ParseError(e));
                } catch (JSONException e) {
                    return Response.error(new ParseError(e));
                }
            }

            @Override
            protected void deliverResponse(JSONObject response) {
                super.deliverResponse(response);
//                    Log.e("VolleyError ", "" + response);
            }

            @Override
            public void deliverError(VolleyError error) {
                super.deliverError(error);
            }

            @Override
            protected VolleyError parseNetworkError(VolleyError volleyError) {
                return super.parseNetworkError(volleyError);
            }
        };
        VolleySingleton.getInstance(this).addToRequestQueue(jsonObjReq);
*/
    }

    @Override
    protected void onResume() {
        super.onResume();
        callCameraList();

//        getAccessToken();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != RESULT_OK ) return;
        // Check which request we're responding to
        if (requestCode == IMAGE_REQUEST_CODE) {
            Uri uri = null;
            if (data != null) {
                uri = data.getData();
            }
            if (uri == null && mCameraFileName != null) {
                uri = Uri.fromFile(new File(mCameraFileName));
            }
            File file = new File(mCameraFileName);

            if (uri != null) {
                UploadPicture upload = new UploadPicture(this, mApi,
                        PHOTO_DIR, file);
                upload.execute();
            }
        }
    }


    public void getAccessToken() {
        if (mApi.getSession().authenticationSuccessful()) {
            try {
                // Required to complete auth, sets the access token on the session
                mApi.getSession().finishAuthentication();
                ACCESS_TOKEN = mApi.getSession().getOAuth2AccessToken();
                if (ACCESS_TOKEN != null) {
                    //Store accessToken in SharedPreferences
                    DataStorage.getInstance().setAccessToken(ACCESS_TOKEN);
                    //Proceed to MainActivity
                }
            } catch (IllegalStateException e) {
                Log.i("DbAuthLog", "Error authenticating", e);

            }
        }
    }

    private void initToolbars() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setPadding(0, 0, 0, 0);
        groupMenu = (ImageView) findViewById(R.id.add_camera);
        groupMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*Intent mIntentVideo = new Intent(MainActivity.this, ScannerActivity.class);
                startActivity(mIntentVideo);*/
                Intent intent = new Intent(MainActivity.this, WifiListActivity.class);
                startActivity(intent);
            }
        });
    }

}
