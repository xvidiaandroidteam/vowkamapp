package com.xvidia.net.jimiwifi.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.xvidia.net.jimiwifi.R;

/**
 * Created by vasu on 7/3/17.
 */
public class SharedForOthersFragment extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_shared_for_others, container, false);

        return rootView;
    }
}
