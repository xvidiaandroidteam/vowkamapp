package com.xvidia.net.jimiwifi.network.model;

/**
 * Created by vasu on 3/5/16.
 */
public class ModelManager {

    private static ModelManager instance = null;
    public static ModelManager getInstance() {
        if (instance == null) {
            instance = new ModelManager();
        }
        return instance;
    }

    private ModelManager(){

    }
    public Trip getTrip() {
        return trip;
    }

    public void setTrip(Trip trip) {
        this.trip = trip;
    }

    private Trip trip;

    public TripDetails getTripDetails() {
        return tripDetails;
    }

    public void setTripDetails(TripDetails tripDetails) {
        this.tripDetails = tripDetails;
    }

    private TripDetails tripDetails;

    public PassengerVO getPassengerVO() {
        return passengerVO;
    }

    public void setPassengerVO(PassengerVO ownProfile) {
        this.passengerVO = ownProfile;
    }

    private PassengerVO passengerVO;

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    private Car car;

    public Car getBuddycar() {
        return buddycar;
    }

    public void setBuddycar(Car buddycar) {
        this.buddycar = buddycar;
    }

    private Car buddycar;
}
