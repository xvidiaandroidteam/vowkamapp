package com.xvidia.net.jimiwifi.utils;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

import com.xvidia.net.jimiwifi.R;

/**
 * Created by Ravi_office on 18-Jan-16.
 */
public class CustomFontUtils {

    public static final String ANDROID_SCHEMA = "http://schemas.android.com/apk/res/android";

    public static void applyCustomFont(TextView customFontTextView, Context context, AttributeSet attrs) {
        TypedArray attributeArray = context.obtainStyledAttributes(
                attrs,
                R.styleable.CustomFontTextView);

        String fontName = attributeArray.getString(R.styleable.CustomFontTextView_font);

        // check if a special textStyle was used (e.g. extra bold)
        int textStyle = attributeArray.getInt(R.styleable.CustomFontTextView_textStyle, 0);

        // if nothing extra was used, fall back to regular android:textStyle parameter
        if (textStyle == 0) {
            textStyle = attrs.getAttributeIntValue(ANDROID_SCHEMA, "textStyle", Typeface.NORMAL);
        }

        Typeface customFont = selectTypeface(context, fontName, textStyle);
        customFontTextView.setTypeface(customFont);

        attributeArray.recycle();
    }

    private static Typeface selectTypeface(Context context, String fontName, int textStyle) {
        if(fontName!=null) {
            if (fontName.contentEquals(context.getString(R.string.font_name_normal))) {
                return FontCache.getTypeface("fonts/Roboto-Regular.ttf", context);
            } else if (fontName.contentEquals(context.getString(R.string.font_name_thin))) {
                return FontCache.getTypeface("fonts/Roboto-Thin.ttf", context);
            } else if (fontName.contentEquals(context.getString(R.string.font_name_medium))) {
                return FontCache.getTypeface("fonts/Roboto-Medium.ttf", context);
            } else if (fontName.contentEquals(context.getString(R.string.font_name_bold))) {
                return FontCache.getTypeface("fonts/Roboto-Bold.ttf", context);
            } else if (fontName.contentEquals(context.getString(R.string.font_name_light))) {
                return FontCache.getTypeface("fonts/Roboto-Light.ttf", context);
            } else if (fontName.contentEquals(context.getString(R.string.font_logo_bold))) {
                return FontCache.getTypeface("fonts/VAG_Rounded_Bold_0.ttf", context);
            } else {
                return FontCache.getTypeface("fonts/Roboto-Regular.ttf", context);
            }
        }else {
            return FontCache.getTypeface("fonts/Roboto-Regular.ttf", context);
        }
    }
}
