package com.xvidia.net.jimiwifi.utils;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.os.Build;
import android.support.v4.content.PermissionChecker;

import com.xvidia.net.jimiwifi.MyApplication;

import java.util.List;

/**
 * Created by vasu on 5/10/16.
 */

public class Utils {

    private static  Utils instance = null;
    public static  Utils getInstance() {
        if (instance == null) {
            instance = new Utils();
             }
        return instance;
    }

    public boolean addPermission(List<String> permissionList, String permission) {
// For Android < Android M, self permissions are always granted.
        boolean result = true;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            if (getTargetSdkVersion() >= Build.VERSION_CODES.M) {
// targetSdkVersion >= Android M, we can
// use Context#checkSelfPermission
                result = MyApplication.getAppContext().checkSelfPermission(permission)
                        == PackageManager.PERMISSION_GRANTED;
            } else {
// targetSdkVersion < Android M, we have to use PermissionChecker
                result = PermissionChecker.checkSelfPermission(MyApplication.getAppContext(), permission) == PermissionChecker.PERMISSION_GRANTED;
            }
        }
        if (!result) {
            permissionList.add(permission);
        }

        return result;
    }

    public int getTargetSdkVersion() {
        int targetSdkVersion = 22;
        try {
            final PackageInfo info = MyApplication.getAppContext().getPackageManager().getPackageInfo(
                    MyApplication.getAppContext().getPackageName(), 0);
            targetSdkVersion = info.applicationInfo.targetSdkVersion;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return targetSdkVersion;
    }


}
